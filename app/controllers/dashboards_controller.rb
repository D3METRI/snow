class DashboardsController < ApplicationController
before_filter :authenticate_user!, only: :index
  def index
  	@partial = current_user.userlevel
  	

  	#CUSTOMER
  	@cuser = current_user if current_user.userlevel == "customer"
  	@homes = @cuser.homes.all
  	@home = @homes.first
  	@special = @home.specials.all

  end
end
