json.extract! visit, :id, :toilet, :tap, :car, :thermostat, :walkaround, :damage, :note, :visittime, :home_id, :user_id, :created_at, :updated_at
json.url visit_url(visit, format: :json)
