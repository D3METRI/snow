json.extract! home, :id, :user_id, :street, :city, :zipcode, :lat, :lng, :created_at, :updated_at
json.url home_url(home, format: :json)
