json.extract! special, :id, :title, :body, :home_id, :created_at, :updated_at
json.url special_url(special, format: :json)
