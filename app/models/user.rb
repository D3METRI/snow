class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :visits
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

	enum userlevel: [:inactive, :worker, :customer]
	has_many :homes
	accepts_nested_attributes_for :homes
end
