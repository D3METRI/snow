class Visit < ActiveRecord::Base
  belongs_to :home
  belongs_to :user

  #enums
enum tap: [:tap_yes, :tap_no, :tap_na]
enum toilet: [:toilet_yes, :toilet_no, :toilet_na]
enum thermostat: [:thermostat_yes, :thermostat_no, :thermostat_na]
enum walkaround: [:walkaround_yes, :walkaround_no, :walkaround_na]
enum damage: [:damage_yes, :damage_no, :damage_na]
enum car: [:car_start, :car_move, :car_no, :car_na] 
end
