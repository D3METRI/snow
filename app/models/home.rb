class Home < ActiveRecord::Base
  belongs_to :user
  has_many :specials
  has_many :visits
geocoded_by :address, :latitude  => :lat, :longitude => :lng
after_validation :geocode 

def address
  [street, city, state, zipcode].compact.join(', ')
end


end
