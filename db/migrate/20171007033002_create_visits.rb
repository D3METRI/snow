class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.integer :toilet
      t.integer :tap
      t.integer :car
      t.integer :thermostat
      t.integer :walkaround
      t.integer :damage
      t.string :note
      t.datetime :visittime
      t.references :home, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
