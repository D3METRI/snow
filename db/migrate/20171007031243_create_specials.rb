class CreateSpecials < ActiveRecord::Migration
  def change
    create_table :specials do |t|
      t.string :title
      t.string :body
      t.references :home, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
