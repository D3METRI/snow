class CreateHomes < ActiveRecord::Migration
  def change
    create_table :homes do |t|
      t.references :user, index: true, foreign_key: true
      t.string :street
      t.string :city
      t.string :state
      t.integer :zipcode
      t.float :lat
      t.float :lng

      t.timestamps null: false
    end
  end
end
